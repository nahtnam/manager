# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :manager,
  ecto_repos: [Manager.Repo]

# Configures the endpoint
config :manager, Manager.Web.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "gXay0K/ks9wgeLsnOZS0iJsvaqppRxRNJUQoTQhhStAaKgy/H0qEsfk5VgI0bPnw",
  render_errors: [view: Manager.Web.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Manager.PubSub,
           adapter: Phoenix.PubSub.PG2]

config :guardian, Guardian,
  issuer: "manager",
  ttl: { 30, :days },
  allowed_drift: 2000,
  secret_key: "gXay0K/ks9wgeLsnOZS0iJsvaqppRxRNJUQoTQhhStAaKgy/H0qEsfk5VgI0bPnw",
  serializer: Manager.Web.Auth.GuardianSerializer

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
