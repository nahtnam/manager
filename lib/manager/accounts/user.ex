defmodule Manager.Accounts.User do
  use Ecto.Schema
  import Ecto.Changeset
  alias Manager.Accounts.User


  schema "accounts_users" do
    field :efficiency, :decimal
    field :email, :string
    field :first_name, :string
    field :hours, :integer
    field :is_admin, :boolean, default: false
    field :last_name, :string
    field :password, :string, virtual: true
    field :password_hash, :string

    timestamps()
  end

  def display_name(%Manager.Accounts.User{first_name: first_name, last_name: last_name}) do
    "#{first_name} #{last_name}"
  end

  def display_name(%Manager.Accounts.User{first_name: first_name}) do
    first_name
  end

  def display_name(%Manager.Accounts.User{last_name: last_name}) do
    last_name
  end

  def display_name(%Manager.Accounts.User{email: email}) do
    email
  end

  @doc false
  def changeset(%User{} = user, attrs) do
    user
    |> cast(attrs, [:first_name, :last_name, :email, :is_admin, :hours, :efficiency])
    |> validate_required([:first_name, :last_name, :email])
  end

  def registration_changeset(struct, params) do
    struct
    |> changeset(params)
    |> cast(params, ~w(password)a, [])
    |> validate_length(:password, min: 6, max: 100)
    |> hash_password
  end

  defp hash_password(changeset) do
    case changeset do
      %Ecto.Changeset{valid?: true,
                      changes: %{password: password}} ->
        put_change(changeset,
                   :password_hash,
                   Comeonin.Bcrypt.hashpwsalt(password))
      _ ->
        changeset
    end
  end
end
