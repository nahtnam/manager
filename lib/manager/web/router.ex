defmodule Manager.Web.Router do
  use Manager.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :with_session do
    plug Guardian.Plug.VerifySession
    plug Guardian.Plug.LoadResource
    plug Manager.Web.Auth.CurrentUser
  end

  pipeline :login_required do
    plug Guardian.Plug.EnsureAuthenticated, handler: Manager.Web.Auth.GuardianErrorHandler
  end

  pipeline :admin_required do
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", Manager.Web do
    pipe_through [:browser, :with_session] # Use the default browser stack
    get "/", PageController, :index

    resources "/users", UserController, only: [:new, :create]
    resources "/sessions", SessionController, only: [:new, :create]

    scope "/" do
      pipe_through [:login_required]
      get "/users/edit", UserController, :edit
      put "/users/update", UserController, :update
      get "/sessions/logout", SessionController, :delete

      scope "/admin", Admin, as: :admin do
        pipe_through [:admin_required]
      end
    end
  end

  # Other scopes may use custom stacks.
  # scope "/api", Manager.Web do
  #   pipe_through :api
  # end
end
