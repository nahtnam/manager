defmodule Manager.Repo.Migrations.CreateManager.Accounts.User do
  use Ecto.Migration

  def change do
    create table(:accounts_users) do
      add :first_name, :string
      add :last_name, :string
      add :email, :string, null: false
      add :password_hash, :string
      add :is_admin, :boolean, default: false, null: false
      add :hours, :integer
      add :efficiency, :decimal

      timestamps()
    end

    create unique_index(:accounts_users, [:email])
  end
end
